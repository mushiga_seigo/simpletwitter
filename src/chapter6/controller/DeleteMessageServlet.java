package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

@WebServlet( "/deleteMessage" )
public class DeleteMessageServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		String messageId = request.getParameter("message_id");
		Integer deleteMessageId = Integer.parseInt(messageId);
		new MessageService().delete(deleteMessageId);
		response.sendRedirect("./");
    }
}
