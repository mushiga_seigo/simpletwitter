package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet( "/edit" )
public class EditMessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		//URLチェック
		String messageId = request.getParameter("message_id");
		List<String> errorMessages = new ArrayList<String>();
        Message editMessage = null;

        if ((StringUtils.isNotBlank(messageId)) && (messageId.matches("^[0-9]+$"))) {
        	editMessage = new MessageService().selectMessage(messageId);
		}

        if (editMessage == null) {
			errorMessages.add("不正なパラメータが入力されました");
		}

        if (errorMessages.size() != 0) {
        	request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./").forward(request, response);
            return;
        }
        request.setAttribute("message", editMessage);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        Message editmessage = getMessage(request);

        String text = request.getParameter("text");
        if (isValid(text, errorMessages)) {
        	try {
        		new MessageService().update(editmessage);
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

        if (errorMessages.size() != 0) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", editmessage);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }
        session.setAttribute("message", editmessage);
        response.sendRedirect("./");
    }

	private Message getMessage(HttpServletRequest request) {
		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("id")));
		message.setUserId(Integer.parseInt(request.getParameter("user_id")));
		message.setText(request.getParameter("text"));
        return message;
	}

	private boolean isValid(String text, List<String> errorMessages) {
        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}