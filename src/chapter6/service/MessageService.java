package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String startDate, String endDate) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;

        try {
        	connection = getConnection();

            //userIdがnullでなければその値をidに代入
            Integer id = null;
            if(!StringUtils.isBlank(userId)) {
            	id = Integer.parseInt(userId);
            }

            if (!StringUtils.isBlank(startDate)) {
            	startDate += " 00:00:00";
            } else {
            	String defaultDate = "2020-01-01 00:00:00";
            	startDate = defaultDate;
            }

            if (!StringUtils.isBlank(endDate)) {
            	endDate += " 23:59:59";
            } else {
            	//現在日時を取得
                Date dateObj = new Date();
        		SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        		// 日時情報を指定フォーマットの文字列で取得
        		String nowDate = format.format( dateObj );
            	endDate = nowDate;
            }

			List<UserMessage> messages = new UserMessageDao().select(connection, id, startDate, endDate, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void delete(Integer messageId) {
		Connection connection = null;

        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public Message selectMessage(String messageId) {
		Connection connection = null;
		connection = getConnection();
        try {
        	Message editMessage = new MessageDao().selectMessage(connection, messageId);
            commit(connection);
            return editMessage;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public void update(Message editmessage) {
		Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, editmessage);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}